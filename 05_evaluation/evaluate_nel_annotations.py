import argparse
import sys, os
import sqlite3
import json
import pandas as pd


def read_database(database_file, project_id):
    # Connecting to sqlite
    # connection object
    connection_obj = sqlite3.connect(database_file)

    # cursor object
    cursor_obj = connection_obj.cursor()

    # to select all column we will use

    statement = '''
    select task_completion.task_id, task.data, task_completion.result, prediction.result from task_completion, task, prediction where task.project_id=? and task.id=task_completion.task_id and task.id=prediction.task_id;
    '''

    cursor_obj.execute(statement, (project_id,))

    annotations = {}
    output = cursor_obj.fetchall()
    for row in output:
        annotations[row[0]] = (json.loads(row[1]), json.loads(row[2]), json.loads(row[3]))

    connection_obj.commit()

    # Close the connection
    connection_obj.close()
    return annotations

def make_dataframe(annotations, opentapioca):
    data = []
    print("opentapioca keys", opentapioca.keys())
    for annotation in annotations.values():
        text, true_labels, pred_labels = annotation
        filename = text.get("document")
        if filename not in opentapioca.keys():
            print("Skipping", filename, "not found in OpenTapioca predictions")
            continue
        labels_tapioca = (opentapioca.get(filename, {}).get('choices'))
        options = dict([(v.get('value'), v.get('html').split('-')[0]) for v in text.get("options")])
    #    print('options', options)
        labels_true = [v.get("value", {}).get("choices") for v in true_labels if "choices" in v.get("value",{})]
        if labels_true:
            labels_true = labels_true[0]
        labels_pred = [v.get("value", {}).get("choices") for v in pred_labels if "choices" in v.get("value",{})]
        if labels_pred:
            labels_pred = labels_pred[0]
#        print(filename)
#        print("labels_tapi", sorted(labels_tapioca))
#        print("labels_true", sorted(labels_true))
#        print("labels_pred", sorted(labels_pred))
        for k, v in options.items():
            data.append({"filename": filename, "qname": k, "entity": v, "annotation": k in labels_true, "prediction": k in labels_pred, "opentapioca": k in labels_tapioca, "correct_my": (k in labels_pred) == (k in labels_true), "correct_tapioca": (k in labels_tapioca) == (k in labels_true)})

    df = pd.DataFrame(data)
    print(df['correct_my'].groupby(df['correct_my']).count())
    print(df['correct_tapioca'].groupby(df['correct_tapioca']).count())
    return df

def main():
    parser = argparse.ArgumentParser(description="Evaluate NEL annotations")
    parser.add_argument("database_file", help="Path to the SQLite database file")
    parser.add_argument("project_id", help="Project ID to evaluate")
    parser.add_argument("opentapioca_files", help="Directory with OpenTapioca predictions in JSON")
    parser.add_argument("output_dirname", help="Directory to save the output")
    args = parser.parse_args()

    database_file = args.database_file
    project_id = args.project_id
    opentapioca_files = args.opentapioca_files
    output_dirname = args.output_dirname

    if not os.path.exists(database_file):
        print(f"Database file {database_file} does not exist.")
        sys.exit(1)

    if not os.path.exists(opentapioca_files):
        print(f"Directory with OpenTapioca files {opentapioca_files} does not exist.")
        sys.exit(1)

    opentapioca = {}
    for opentapioca_file in os.listdir(opentapioca_files):
        with open(os.path.join(opentapioca_files, opentapioca_file), 'r', encoding='utf-8') as f:
            opentapioca_json = json.load(f)
            filename = os.path.basename(opentapioca_file).split('.')[0]
            try:
                opentapioca[filename] = opentapioca_json.get('predictions')[0]["result"][-1].get('value')
            except:
                pass

    if not opentapioca.keys():
        print("OpenTapioca predictions not found")
        sys.exit(1)

    print("OpenTapioca predictions", opentapioca)

    annotations = read_database(database_file, project_id)

    if not os.path.exists(output_dirname):
        os.makedirs(output_dirname, exist_ok=True)

    df = make_dataframe(annotations, opentapioca)

    with open(output_dirname + '/annotation_prediction.csv', 'w', encoding='utf-8') as f:
        df.to_csv(f, sep='\t')

if __name__ == "__main__":
    main()
