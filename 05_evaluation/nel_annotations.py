import sys, os
import sqlite3
import json
import pandas as pd

if len(sys.argv)<4:
   print("Usage: nel_annotations.py <database_file> <project_id> <output_file>")
   sys.exit(0)

database_file = sys.argv[1]
project_id = sys.argv[2]
output_filename = sys.argv[3]

print("Reading ground truth from db, and saving to", output_filename)

# Connecting to sqlite
# connection object
connection_obj = sqlite3.connect(database_file)

# cursor object
cursor_obj = connection_obj.cursor()

# to select all column we will use

statement = '''
select task_completion.task_id, task.data, task_completion.result, prediction.result from task_completion, task, prediction where task.project_id=? and task.id=task_completion.task_id and task.id=prediction.task_id;
'''

cursor_obj.execute(statement, (project_id,))

annotations = {}
output = cursor_obj.fetchall()
for row in output:
    annotations[row[0]] = (json.loads(row[1]), json.loads(row[2]), json.loads(row[3]))

connection_obj.commit()

# Close the connection
connection_obj.close()

data = []
for annotation in annotations.values():
    text, true_labels, pred_labels = annotation
    filename = text.get("document")
    labels_tapioca = []
    if os.path.exists('res/opentapioca/'+filename+'.json'):
        opentapioca = json.load(open('res/opentapioca/'+filename+'.json')).get('predictions')[0]["result"][-1].get('value')
        labels_tapioca = sorted(opentapioca.get('choices'))
    options = dict([(v.get('value'), v.get('html').split('-')[0]) for v in text.get("options")])
#    print('options', options)
    labels_true = [v.get("value", {}).get("choices") for v in true_labels if "choices" in v.get("value",{})]
    if labels_true:
        labels_true = sorted(labels_true[0])
    labels_pred = [v.get("value", {}).get("choices") for v in pred_labels if "choices" in v.get("value",{})]
    labels_pred = sorted(labels_pred[0])
    print(filename)
    for k, v in options.items():
        data.append({"filename": filename, "qname": k, "entity": v, "annotation": k in labels_true, "prediction": k in labels_pred, "opentapioca": k in labels_tapioca, "correct_my": (k in labels_pred) == (k in labels_true), "correct_tapioca": (k in labels_tapioca) == (k in labels_true)})

df = pd.DataFrame(data)
print(df['correct_my'].groupby(df['correct_my']).count())
print(df['correct_tapioca'].groupby(df['correct_tapioca']).count())
df.to_csv(open(output_filename, 'w'), sep='\t')
