import sys, os
from nervaluate import Evaluator
import pandas as pd

true_dir=sys.argv[1]
pred_dir=sys.argv[2]
print("Calculate MUC evaluation for", pred_dir, "Truth=",true_dir)

true = {}
pred = {}
for root, dirs, filenames in os.walk(true_dir):
   for filename in filenames:
      if filename.endswith('.iob'):
         pred_file = os.path.join(pred_dir, filename)
         if not os.path.exists(pred_file):
            print("Prediction does not exist", pred_file)
         with open(os.path.join(root, filename)) as f, open(pred_file) as g:
            y_true = [x.split('\t')[-1].strip() for x in f.readlines()]
            y_pred = [x.split('\t')[-1].strip() for x in g.readlines()]
            true[filename] = y_true
            pred[filename] = y_pred



evaluator = Evaluator(list(true.values()), list(pred.values()), tags=['LOC', 'PER', 'ORG', 'MISC'], loader="list")

#print("true", list(true.values())[0])
#print("pred", list(pred.values())[0])

#results, results_by_tag = evaluator.evaluate()
results, results_per_tag, result_indices, result_indices_by_tag = evaluator.evaluate()

print(pd.DataFrame(results).to_latex(float_format="%.2f"))

print("\n\nBy tag")
for k in results_per_tag.keys():
   print("\subsubsection{"+k+"}")
   print(pd.DataFrame(results_per_tag[k]).to_latex(float_format="%.2f"))

print("Files", len(true))
