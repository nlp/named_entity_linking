# Annotation manual

The NER annotation describes four classes: PERson, ORGanization, LOCation, and MISCellaneous. The aims are:
- to make the annotation as consistent accross languages as possible,
- to prefer "linkable" entities, i.e., entities that most likely appear or could appear in an encyclopedia, ontology, address book etc.

## PERSon
Positive examples:
- person name (Barack Obama) or alias (50Cent)
- person's (social media) user names (@ZuzkaNeverilova)
- Czech possessive from person name (Guzmánův)
- divine names: God, Alláh, Thor, Perun

Negative examples:
- Mr. Know-it-all - not a particular person
- individual roles: President, Pope
- honorific and academic titles: Dr.

Notes:
- Annotate English possessive with "'s" = annotate complete word (*John's* instead of *John*'s)

Difficult cases:
- Sometimes, social media user names belong to a person or an organization and it is indistiguishable.


# ORGanization

Positive examples:
- political party (National party)
- state authorities (Ethiopian Federal Police)
- associations (FIFA)
- musical/theatre/artistic group (Czech National Theatre)
- place name as metonym for organizations (White House, Kremlin)
- company name, even incomplete (Facebook, Twitter, BBC)

Negative examples:
- Police, Parliament unless they refer to particular organization
- a London university (one of many)

Notes:
- Do not annotate determiners unless they are really part of the ORG name.

Difficult cases:
- Metonymy: Some occurrences seem to be LOCations, but in the context, the ORGanization is meant, e.g., Kremlin decided something (not the place, but the ORGanization located there).
- Informal groups: It is always a question if e.g., a named group of people should be an organization. The "linkable" rule should apply.
- Media name: Sometimes, the ORGanization is not distinguishable from the platform it runs. E.g., are "Facebook rules" the rules of the company or the platform. We prefer the latter.


# LOCation
Positive examples:
- geonames (countries, cities, rivers, continents)
- individual areas (e.g., West Africa, even in Czech, západní Afrika)
- position, building complex (Maekalawi detention center, cafe Socrates)
- building name (Goce Delchev student dorm, near Hospital Amitie)
- International Space Station (ISS)

Negative examples:
- adjectives are annotated as MISC in English, and no entity in Czech

## MISCellaneous
Positive examples:
- nation name (German, American, Chilean) in English, in Czech only if it is a noun (Čech)
- language name (French, Mandarin) only in English, in Czech language names are not an entity
(francouzština, mandarínština)
- national origin adjective only in English (Czech - MISC, český - no entity)
- religion/church name (Catholic, křesťan), in Czech only if noun (křesťanský - no entity)
- media, social network, platform (New York Times, El Ciudadano, Facebook, tumblr, pri.org), adjectives in Czech are not entities (facebooková stránka - no entity), verbs are not entities (tweeted, tweetoval) 
- license name, including version (CC BY-NC-ND 2.0)
- software name, including version (Gmail)
- product name, including type and version (Nike)
- artwork name (Dune 2, DDoS, Distributed Denial of Service)
- event name (Women’s Day; Ayotzinapa as metonymy for Ayotzinapa incident; Movember)
- award, competition, if annual, omit the year (Inspiring Women Award 2015 -> Inspiring Women Award; Winter Olympic Games 1998 -> Winter Olympic Games)

## NO ENTITY
- currencies (USD)
- language codes (ru, eng)
- law names
- common words often written capitalized (e.g., Parliament, Commision) unless they are abbreviations for particular organizations (European Commision)
- internet
- pro- anti- (pro-Beijing, anti-China), -based (Mexico-based)
- acronyms such as LGBT, PM (parliament member), AIDS
- Mr. Know-it-All

