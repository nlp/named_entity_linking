import sys, os
import sqlite3
import json

if len(sys.argv)<5:
   print("Usage: extract_document.py <document_name> <database_file> <project_id> <output_dir>")
   sys.exit(0)

document_file = sys.argv[1]
database_file = sys.argv[2]
project_id = sys.argv[3]
output_dir = sys.argv[4]

print("Extract document", document_file)

# Connecting to sqlite
# connection object
connection_obj = sqlite3.connect(database_file)

# cursor object
cursor_obj = connection_obj.cursor()

# to select all column we will use
statement = '''
select data, id from task where project_id = ? and instr(data, ?);
'''
cursor_obj.execute(statement, (project_id, document_file))

json_document = {}

output = cursor_obj.fetchall()
for row in output:
    if row[1]:
        json_document[row[1]] = {"text": json.loads(row[0])}

print(json_document)

statement = '''
select result from task_completion where project_id = ? and task_id=?;
'''
ner_annotations = []
for task_id in json_document.keys():
    cursor_obj.execute(statement, (project_id, task_id))
    output = cursor_obj.fetchone()
    if output:
        json_document[task_id].update({"annotation": json.loads(output[0])})
    else:
        print("Not found", task_id)

connection_obj.commit()

# Close the connection
connection_obj.close()

with open(os.path.join(output_dir, document_file+".json"), "w") as f:
    json.dump(json_document, f, ensure_ascii=False, indent=2)
