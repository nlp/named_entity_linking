import argparse
import sys, os

import json
import spacy
from spacy.language import Language
from sentence_transformers import SentenceTransformer, util

def find_sorted_candidates(span, sentence, model):
    if not span._.annotations:
        return []
    tags = span._.annotations['tags']
    if len(tags) == 1: # no comparison needed
        return find_candidates(tags)
    sentences = [s['desc'][0] for s in span._.annotations['tags'] if s['desc']]
#    print("Find similarities of")
#    print(sentences)
#    print("AND")
#    print("TEXT", sentence)
    reference_embeddings = model.encode([sentence], convert_to_tensor=True)
    embeddings = model.encode(sentences, convert_to_tensor=True)
    try:
        cosine_scores = util.cos_sim(embeddings, reference_embeddings)
        # Output the pairs with their score
        sentence_scores = []
        for i in range(len(sentences)):
            #print(sentences[i], sentence, cosine_scores[i][0], tags[i]['score'], cosine_scores[i][0]*tags[i]['score'])
            sentence_scores.append((tags[i], cosine_scores[i][0]*tags[i]['score']))
        sorted_tags = [x[0] for x in sorted(sentence_scores, key=lambda x: x[1], reverse=True)]

    #    tags = sorted(span._.annotations['tags'], key=lambda x:x['score'], reverse=True)

        return find_candidates(sorted_tags)
    except:
        return []

def find_candidates(tags):
    qname_definition = {}
    for annotation in tags:
#         print(annotation['id'], annotation['label'], annotation['desc'], annotation['score'])
#        print(span.id_, span.text, span.kb_id_, span.label_, span._.description)
#        print(span._.aliases)
        if annotation['score']>0 and annotation['desc']: # TODO: is scoring needed here?
            #print(annotation['id'], annotation['label'], annotation['desc'], annotation['score'])
            qname_definition[annotation['id']] = (annotation['label'], annotation['desc'][0])
    return qname_definition

def find_qnames(sent, start, label, mappings, model):
    qnames = None
    for ent in sent.ents:
        sentence_start_char = ent.start_char - ent.sent.start_char
        if start==sentence_start_char and (ent.label_, label) in mappings:
#            print("ENTITY", ent, ent.kb_id_, ent.label_, "start sent",sentence_start_char)
            return find_sorted_candidates(ent, sent, model)
    return None

def get_aliases(doc):
    ents_aliases = {}
    qnames = {}
    for span in doc.ents:
#        print("Tags", span._.annotations)
        if not span._.annotations:
            continue
#        candidates = find_candidates(span._.annotations['tags'])
#        if candidates:
#            qnames.update(candidates)
        # original Tapioca algorithm
        if span._.description:
            qnames.update({span.kb_id_: (span.label_, span._.description[0])})
        """
        for annotation in span._.annotations['tags']:
            if annotation['score']>0 and annotation['desc']: # TODO: is scoring needed here?
                aliases = annotation['aliases']
                if annotation['types'].get('Q5'): # for humans, add first and last token of their name
                    aliases += [annotation['label'].split()[0], annotation['label'].split()[-1]]
                for alias in aliases:
                    if alias not in ents_aliases:
                        ents_aliases[alias] = []
                    if annotation['id'] not in ents_aliases[alias]:
                        ents_aliases[alias].append(annotation['id'])
        """
    return ents_aliases, qnames

def get_annotations(doc, json_document, lang, mappings, model, ents_aliases, qnames):
    annotations = []
    for sent, annot in zip(doc.sents, json_document.values()):
        sent_annot = []
        for annot in annot.get('annotation'):
            if annot.get('to_name')==lang:
                a = annot.get('value')
                text = a.get('text')
    #            print("annotation", annot.get('value'))
                q = find_qnames(sent, a.get('start'), a.get('labels')[0], mappings, model)
                if q:
                    a['qnames'] = q
                if not q and text in ents_aliases.keys():
                    a['qnames'] = dict([(c,qnames[c]) for c in ents_aliases[text]])
                a['start_char'] = sent.start_char+a.get('start')
                sent_annot.append(a)
        annotations.append(sent_annot)
    return annotations


def get_qnames(annotations, doc):
    options = {}
    labels = {}
    predictions = []
    selected = []
    coords = []
    i = 0
    for sent in annotations:
        for annot in sent:
            if annot.get('qnames'):
    #            print("Annot", annot)
                j = 0
                for qname, desc in annot.get('qnames').items():
                    # strange fix
                    if annot.get("text") != doc.text[annot.get("start_char"):annot.get("start_char")+len(annot.get("text"))]:
                         print("Bad annotation coords", annot)
    #                    annot["start_char"] = annot["start_char"] - 1
                    elif annot["start_char"] not in coords:
                        predictions.append({"id": "annot"+str(i),
                                            "from_name": "qname",
                                            "to_name": "text",
                                            "type": "labels",
                                            "value": {"start": annot.get("start_char"),
                                                      "end": annot.get("start_char")+len(annot.get("text")),
                                                      "text": annot.get("text"),
                                                      "score": 1,
                                                      "labels": [desc[0]]}})
                        i += 1
                        coords.append(annot["start_char"])
                    if qname not in options.keys():
                        html = ' - '.join(desc)
                        if annot.get("text") not in desc[0]:
                            html = annot.get("text") + ' - ' + html
                        options[qname] = {"value": qname, "html": html.strip()}
                        if j==0: # only the first label is what the classifier has selected
                            labels[desc[0]] = {"value": desc[0]}
                            selected.append(qname)
                            #print("sent annot", annot.get("text"), qname, desc[0])
                        j += 1


    doc_annotations = {"document": doc_id,
                       "text": doc.text.replace('\n', ' '),
                       "options": sorted(options.values(), key=lambda x: x.get('html')),
                       "labels": list(labels.values())}
    #TODO: labels must be qnames, not strings


    predictions.append({
                  "from_name": "dynamic_choices",
                  "to_name": "text",
                  "type": "choices",
                  "value": { "choices": selected}})
    result = {"data":doc_annotations,
              "predictions": [{"model_version": "test", "score": 0.5, "result": predictions}]} #,
    #          "annotations": [{"result": [{
    #                        "from_name": "dynamic_choices",
    #                        "to_name": "text",
    #                        "type": "choices",
    #                        "value": { "choices": selected}
    #          }]}]}
    return result


def main(argv, model_name):
    parser = argparse.ArgumentParser(description='NEL Annotation. Input is a JSON file with NER annotations obtained from LabelStudio.')
    parser.add_argument('ner_annotated_json', type=str, help='Path to the NER annotated JSON file')
    parser.add_argument('output_dir', type=str, help='Output directory')
    args = parser.parse_args()

    ner_annotated_json = args.ner_annotated_json
    output = args.output_dir

    doc_id = ner_annotated_json.split('/')[-1].split('.')[0]

    with open(ner_annotated_json, 'r', encoding='utf-8') as f:
        json_document = json.load(f)

    if not os.path.exists(output):
        os.makedirs(output, exist_ok=True)

    print("Take annotation", ner_annotated_json, 'doc_id', doc_id, "and generate annotation task in", output)

    nlp = spacy.load(model_name)
    config = {"punct_chars": ["\n"], 'overwrite': True}
    # Use a pipeline as a high-level helper
    nlp.add_pipe("sentencizer", config=config)
    nlp.add_pipe('opentapioca', config={"url": "https://opentapioca.org/api/annotate"}) # replace with local server
    doc = nlp("Christian Drosten works in Germany.")

    lang = "eng_text"

    mappings = [('PERSON', 'PER'), ('ORGLOC', 'ORG'), ('ORGLOC', 'LOC'), ('ORG', 'ORG'), ('LOC', 'LOC'), ('ORG', 'MISC')]

    print('\n'.join([v.get('text').get(lang) for k, v in json_document.items()]))

    doc = nlp('Christian Drosten works in Germany.')
    for span in doc.ents:
        print((span.text, span.kb_id_, span.label_, span._.description, span._.score))
    # ('Christian Drosten', 'Q1079331', 'PERSON', 'German virologist and university teacher', 3.6533377082098895)
    # ('Germany', 'Q183', 'LOC', 'sovereign state in Central Europe', 2.1099332471902863)
    ## Check also span._.types, span._.aliases, span._.rank

 #   doc = nlp('\n'.join([v.get('text').get(lang) for k, v in json_document.items()]))

    ents_aliases, qnames = get_aliases(doc)
    model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
    annotations = get_annotations(doc, json_document, lang, mappings, model, ents_aliases, qnames)
    result = get_qnames(annotations, doc)

    with open(os.path.join(output, ner_annotated_json.split('/')[-1]), 'w', encoding='utf-8') as f:
        json.dump(result, f, indent=2, ensure_ascii=False)

def check_spacy_model(model_name):
    try:
        spacy.load(model_name)
        print(f"Model {model_name} is already installed.")
        return True
    except OSError:
        print(f"Model {model_name} is not installed.")
        return False


if __name__ == "__main__":
    model_name = "en_core_web_sm"
    if not check_spacy_model(model_name):
        spacy.cli.download(model_name)
        print(f"Downloaded spaCy model: {model_name}")
    main(sys.argv, model_name)

