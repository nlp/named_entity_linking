import sys, os
import json
import pandas as pd

if len(sys.argv)<4:
   print("Usage: convert_preannotated_document.py <corpus.csv> <NER-annotation> <output-dir> [prediction-model]")
   sys.exit(0)

corpus = sys.argv[1]
ner_dir = sys.argv[2]
output_dir = sys.argv[3]
model_version = 'model without description'

if len(sys.argv)>4:
   model_version = sys.argv[4]

df = pd.read_csv(corpus, sep='\t', keep_default_na=False)

def convert_label(label):
   labels = {"G": "LOC", "P": "PER", "I": "ORG", "T": "O", "M": "MISC", "N": "O", "A": "LOC"}
   known_labels = list(labels.values())
   if label not in labels and label not in known_labels:
      print("unknown label", label)
   return labels.get(label, label)

def align_annotation(data, lang1, lang2, df, annot):
   index = {lang1: 0, lang2: 0}
   filename = df['filename'].iloc[0].split('.')[0]
   for i, row in df.iterrows():
       sentence = {}
       sentence[lang1] = row['text_'+lang1]
       sentence[lang2] = row['text_'+lang2]
       score = row['score']
       alignment = row['alignment']
       segment_id = row['segment_id']
       predictions = []
       for l in [lang1, lang2]:
           for annotation in annot[l]:
               label = convert_label(annotation['entity_group'])
               word = annotation['word']
               if '|' in word: # discard annotations that span several sentences
                  label = 'O'
               excerpt = sentence[l][annotation['start']-index[l]:annotation['end']-index[l]]
               if annotation['start']>index[l] and annotation['start']<index[l]+len(sentence[l]) and label!='O':
                   if word!=excerpt: # differences might be in spacing
                      if word.replace(' ','')==excerpt.replace(' ',''):
                         word = excerpt
                      else:
                         print(filename)
                         print("Sentence", sentence[l])
                         print("start:end", annotation['start'], annotation['end'], index[l])
                         print("Bad alignment", annotation['word'], "=", excerpt)

                   predictions.append({
                             "from_name": l+"_label", "to_name": l+"_text", "type": "labels", "value":
                             {"start": annotation['start']-index[l], "end": annotation['end']-index[l],
                             "text": word, "labels": [label]}})
       data.append({"data": {"document": filename, "index": i, "score": score, "alignment_type": alignment,
                   lang1+"_text": sentence[lang1], lang2+"_text": sentence[lang2], 'segment_id': segment_id},
                   "predictions": [{"model_version": model_version, "result": predictions}]})
       index[lang1] += (len(sentence[lang1])+1)
       index[lang2] += (len(sentence[lang2])+1)
   return data

def find_annotation(data, sentences, filename, dir):
   lang = [x for x in sentences.columns if x.startswith('text_')]
   lang1 = lang[0].split('_')[-1]
   lang2 = lang[1].split('_')[-1]
   annot = {}
   annot[lang1] = json.load(open(os.path.join(dir, lang1, filename+'.json')))
   annot[lang2] = json.load(open(os.path.join(dir, lang2, filename+'.json')))
   align_annotation(data, lang1, lang2, sentences, annot)

documents = df.groupby(by='filename')

data = []

for filename, sentences in documents:
   find_annotation(data, sentences, filename, ner_dir)
#   break

with open(os.path.join(output_dir, 'preannotations.json'), "w") as f:
    json.dump(data, f, ensure_ascii=False, indent=2)
