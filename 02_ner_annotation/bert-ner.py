import sys
import os
import json
import numpy as np
# Use a pipeline as a high-level helper
from transformers import pipeline, AutoModelForTokenClassification, AutoTokenizer

if len(sys.argv)<4:
   print("Usage: bert-ner.py <dir_with_txt_documents> <NER-model> <output_dir>")
   sys.exit(0)

dir_name = sys.argv[1]
model_name = sys.argv[2]
output_dir = sys.argv[3]

if not os.path.exists(output_dir):
   os.mkdir(output_dir)

print("Process", dir_name, "with", model_name)

#model="dslim/bert-base-NER"

# conversion for Czech models based on CNEC (from 8 labels to 4+O)
id2label={0: 'O', 1: 'I-T', 2: 'I-P', 3: 'I-O', 4: 'I-M', 5: 'I-I', 6: 'I-G', 7: 'I-A', 8: 'B-T', 9: 'B-P', 10: 'B-O', 11: 'B-M', 12: 'B-I', 13: 'B-G', 14: 'B-A'}

if 'robeczech' in model_name:
   model = AutoModelForTokenClassification.from_pretrained(model_name, id2label=id2label)
   tokenizer = AutoTokenizer.from_pretrained("ufal/robeczech-base", add_prefix_space=True, model_max_length=512, is_split_into_words=True)
else:
   model = AutoModelForTokenClassification.from_pretrained(model_name)
   tokenizer = AutoTokenizer.from_pretrained(model_name, model_max_length=512)

pipe = pipeline("token-classification", model = model, tokenizer=tokenizer, aggregation_strategy="max")

for root, dirs, filenames in os.walk(dir_name):
   for filename in filenames:
       if filename.endswith('txt'):
           with open(os.path.join(root, filename)) as f:
               print(filename)
               content = f.read().replace('|', ' ').replace('\n', '|')
               ner = pipe(content)
               for i, k in enumerate(ner):
                   ner[i]['score'] = np.float64(k['score'])
               with open(os.path.join(output_dir, filename.replace('.txt', '.json')), 'w') as f:
                   json.dump(ner, f)

