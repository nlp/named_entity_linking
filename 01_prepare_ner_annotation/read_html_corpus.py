import sys
import os
import pandas as pd
from bs4 import BeautifulSoup as soup

if len(sys.argv)<2:
   print("Usage: read_html_corpus.py <dir_name> [output_dir]")
   sys.exit(0)

dir_name=sys.argv[1]
output_dir = './'

if len(sys.argv)==3:
   output_dir=sys.argv[2]

print("Processing directory", dir_name)

data = []
for root, dirs, filenames in os.walk(dir_name):
    for filename in sorted(filenames, key=lambda x: int(x.split('.')[0].split('-')[-1]) if '-' in x and 'html' in x else 0):
        if filename.endswith('.html'):
            print(filename)
            lang1 = ''
            lang2 = ''
            with open(os.path.join(root, filename)) as f:
                page = soup(f, "html.parser")
                for tr in page.find_all("tr"):
                    row = {}
                    title = tr.get('title', '')
                    score = alignment = ''
                    row['filename'] = filename.replace('.html','').strip()
                    if ';' in title:
                        score_alignment = title.split(';')
                        if len(score_alignment)==2:
                            score, alignment = score_alignment
                            score = score.replace('Score:','').strip()
                            alignment = alignment.replace('SegmentType:', '').strip()
                            try:
                                row['score'] = float(score)
                            except:
                                pass
                            row['alignment'] = alignment
                    if not lang1 and not lang2:
                        for i, th in enumerate(tr.find_all('th')):
                            if i==1:
                                lang1 = th.contents[0]
                            elif i==2:
                                lang2 = th.contents[0]
                    else:
                        segment_id = ''
                        text1 = text2 = ''
                        for i, td in enumerate(tr.find_all('td')):
                            if i==0:
                                segment_id = td.contents[0]
                            elif i==1:
                                text1 = ' '.join(td.contents)
                            elif i==2:
                                text2 = ' '.join(td.contents)
                        row['segment_id'] = segment_id.strip()
                        row['text_'+lang1] = text1.strip()
                        row['text_'+lang2] = text2.strip()
                    if 'segment_id' in row.keys():
                        data.append(row)
df = pd.DataFrame(data)
print("Extracted data")
print(df.head())

output = os.path.join(output_dir, 'corpus.csv')
print("Save to", output)

with open(output, 'w') as f:
    df.to_csv(f, sep='\t')
