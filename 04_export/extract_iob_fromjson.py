import sys, os
import pandas as pd
import json
import spacy

if len(sys.argv)<3:
   print("Usage: extract_iob_fromjson.py <input_dir_json> <input_gt_csv> <output_dir>")
   sys.exit(0)

input_json_dirname = sys.argv[1]
input_gt_csv = sys.argv[2]
output_dirname = sys.argv[3]

if not os.path.exists(output_dirname):
   os.mkdir(output_dirname)

print("Reading",input_json_dirname,"annotation, and saving to", output_dirname)

def annotate(df, annotation, mapping={'M':'MISC', 'I':'ORG', 'P':'PER', 'G':'LOC', 'MISC': 'MISC', 'ORG': 'ORG', 'PER': 'PER', 'LOC':'LOC'}):
    token_ids = []
#    for i, row in df[['start', 'token', 'token_index']].iterrows():
    for i, row in df.iterrows():
        token_ids.append((row['token_index'],row['start'],row['token']))
    ner = dict([(i, 'O') for i in range(len(token_ids))])
    for a in annotation:
#        print(a)
        if a.get("word"):
            start = a.get('start')
            end = a.get('end')
            text = a.get('word')
            label = a.get('entity_group')
            index = [i for i, s, t in token_ids if s==start]
            if not index:
               while not index and start>0:
                  start = start-1
                  index = [i for i, s, t in token_ids if s==start]
            if index:
               index = index[0]
               ent = mapping.get(label,'O')
               if ent!='O':
                  ner[index] = 'B-'+ent
                  for next in range(index+1, index+len(text.split())+1):
                      if next<len(token_ids):
                         if token_ids[next][1]<end:
                            ner[next] = 'I-'+ent
            else:
               print("no token starts at", start)
               print("token ids", token_ids)
    for i, s, t in token_ids:
        if i>0 and t in ["'s", "’s"] and ner[i]!='O':
#            print("Annotate", t, "as", token_ids[i-1], ner[i-1])
            ner[i] = ner[token_ids[i-1][0]].replace('B-', 'I-')
#    print("NER", ner)
    return [(token_ids[i][2], ner_type) for i, ner_type in ner.items()]


df = pd.read_csv(input_gt_csv, sep='\t', keep_default_na=False)

annotations = {}
for filename in os.listdir(input_json_dirname):
    if filename.endswith('json'):
 #      print("filename", os.path.join(input_json_dirname, filename))
       with open(os.path.join(input_json_dirname, filename)) as f:
          annotations[filename] = json.load(f)
#       break
data = {}

#print("annotations", list(annotations.items())[0])

#print("df", df.head())
for filename, annotation in annotations.items():
    print("filename", filename)
    id = filename.split('/')[-1]
    id = id.split('.')[0]
    if filename not in data.keys():
        data[id] = []
    token_labels = []
    for token, label in annotate(df.loc[df.document==id], annotation):
        if token.strip():
            token_labels.append((token, label))
    data[id].append((id, token_labels))

for filename in data.keys():
    sentences = sorted(data[filename], key=lambda x: x[0])
    for i, s in sentences:
        with open(os.path.join(output_dirname, filename+'.iob'), 'a+') as f:
            for token, label in s:
                f.write('{}\t{}\n'.format(token, label))
