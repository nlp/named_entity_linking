import sys, os
import sqlite3
import json
import spacy

if len(sys.argv)<5:
   print("Usage: extract_iob_fromdb.py <language_code> <database_file> <project_id> <output_dir>")
   sys.exit(0)

lang = sys.argv[1]
database_file = sys.argv[2]
project_id = sys.argv[3]
output_dirname = sys.argv[4]

if not os.path.exists(output_dirname):
   os.mkdir(output_dirname)

print("Reading",lang,"ground truth from db, and saving to", output_dirname)

nlp = spacy.blank("en")
nlp.add_pipe("token_splitter")
# Spacy tokenization can differ slightly from what is expected, e.g., entities with dashes can be splitted

def annotate(data, lang, annotation, nlp):
    content = data.get(lang+"_text")
    doc = nlp(content)
    token_ids = dict([(t.idx, t.i) for t in doc])
    ner = dict([(t.i, 'O') for t in doc])
    for a in annotation:
        if a.get('from_name').startswith(lang) and a.get('value').get("text"):
            start = a.get('value').get('start')
            end = a.get('value').get('end')
            text = a.get('value').get('text')
            label = a.get('value').get('labels')[0]
            # find closest start from left
            while content[start]==' ':
                start += 1
                text = text.strip()
#            print("annotation", a)
#            print("content[start:end]", content[start:end], "text", text)
            if len([t for t in doc if t.idx==start])==0: # no token starts at this position
                # find closest token beginning to the left
                while start>0 and len([t for t in doc if t.idx==start])==0:
                    start = start-1
                    text = doc.text[start:end]
            if len([t for t in doc if t.idx==start])==0:
                print("NO TOKEN starts at position", start, "!")
                print("Sentence", content, "NER", content[start:end], "should be", text, "/", label)
            if start in token_ids.keys():
                token_id = token_ids.get(start)
                ner[token_id]='B-'+label
                # this fixes small annotation errors such as marked trailing punctuation, parentheses
                # @ at the beginning of persons ID (e.g., X nickname)
                # most problematic is a trailing comma (e.g., in initial, it is part of the entity, in contrast,
                # an entity at the end of sentence should be without the comma.
                for i in range(token_id+1, len(doc)):
                    if doc[i].idx+len(doc[i].text)<=end:
                        if len(doc[i].text)==1 and doc[i].idx+1==end:
                            if doc[i].text in ',;?-([:v“”_!‘':
#                                print("SKIP the last punctuation at", text)
                                continue
                            elif doc[i].text in ')' and len([t for t in text if t==')'])>len([t for t in text if t=='(']):
#                                print("SKIP the last ) parenthesis at", text)
                                continue
                            elif doc[i].text in ']' and len([t for t in text if t==']'])>len([t for t in text if t=='[']):
#                                print("SKIP the last ] parenthesis at", text)
                                continue
                            elif doc[i].text=='.' and len([t for t in text if t=='.'])==1:
                                if 'CC ' not in text: # license versions
#                                    print("SKIP the last comma at", text)
                                    continue
                        ner[i]='I-'+label
    for t in doc:
        if t.i>0 and t.text=="'s" and ner[t.i]!='O':
#            print("Annotate", t.text, "as", doc[t.i-1], ner[t.i-1], doc.text)
            ner[t.i] = ner[t.i-1].replace('B-', 'I-')
    return [(doc[i].text, ner_type) for i, ner_type in ner.items()]


# Connecting to sqlite
# connection object
connection_obj = sqlite3.connect(database_file)

# cursor object
cursor_obj = connection_obj.cursor()

# to select all column we will use
statement = '''
select task_completion.task_id, task.data, task_completion.result, prediction.result from task_completion, task, prediction where task.project_id=? and task.id=task_completion.task_id and task.id=prediction.task_id;
'''
statement = '''
select task_completion.task_id, task.data, task_completion.result from task_completion, task where task.project_id=? and task.id=task_completion.task_id;
'''

cursor_obj.execute(statement, (project_id,))

annotations = {}
output = cursor_obj.fetchall()
for row in output:
    annotations[row[0]] = (json.loads(row[1]), json.loads(row[2]))

connection_obj.commit()

# Close the connection
connection_obj.close()

data = {}

#print("annotations", list(annotations.items())[0])

for annotation in annotations.values():
    text, true_labels = annotation
    filename = text.get('document')+'.iob'
    if filename not in data.keys():
        data[filename] = []
    token_labels = []
    for token, label in annotate(text, lang, true_labels, nlp):
        if token.strip():
            token_labels.append((token, label))
    data[filename].append((text.get('index'), token_labels))

for filename in data.keys():
    sentences = sorted(data[filename], key=lambda x: x[0])
    for i, s in sentences:
        with open(os.path.join(output_dirname, filename), 'a+') as f:
            for token, label in s:
                f.write('{}\t{}\n'.format(token, label))
