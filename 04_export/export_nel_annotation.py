import os
import sys
import sqlite3
import json
import pandas as pd
import argparse


def read_database(database_file, project_id):
    # Connecting to sqlite
    # connection object
    connection_obj = sqlite3.connect(database_file)

    # cursor object
    cursor_obj = connection_obj.cursor()

    # to select all column we will use

    statement = '''
    select task_completion.task_id, task.data, task_completion.result from task_completion, task where task.project_id=? and task.id=task_completion.task_id;
    '''

    cursor_obj.execute(statement, (project_id,))

    annotations = {}
    output = cursor_obj.fetchall()
    for row in output:
        annotations[row[0]] = (json.loads(row[1]), json.loads(row[2]))

    connection_obj.commit()

    # Close the connection
    connection_obj.close()

    return annotations


def make_dataframe(annotations, df):
    for annotation in annotations.values():
        content, choices = annotation
#        text = content.get('text')
        document = content.get('document')
        if 'eng-' not in document:
            document = document.replace('eng', 'eng-')
        used_labels = [x.get('value', {}).get('choices') for x in choices if x.get('value', {}).get('choices')]
        if used_labels:
            used_labels = used_labels[0]
        options = {}
        for v in content.get('options'):
            if v.get('value') in used_labels:
                for x in v.get('html').split('-')[:-1]:
                    options[x.strip()] = v.get('value')
        annotated_text = [x.get('value', {}) for x in choices if not x.get('value', {}).get('choices')]
    #    print('options', options)
        for annot in annotated_text:
            a = df.loc[(df.document==document)&(df.start>=annot.get('start'))&(df.start<annot.get('end'))]
            if not a.empty:
                qname = options.get(annot.get('text'))
                if not qname:
                    qname = options.get(annot.get('labels')[0])
                df.loc[(df.document==document)&(df.start>=annot.get('start'))&(df.start<annot.get('end')),'nel'] = qname
    return df

def main(argv):
    parser = argparse.ArgumentParser(description='Export NEL annotations.')
    parser.add_argument('database_file', type=str, help='Path to the database file')
    parser.add_argument('project_id', type=str, help='Project ID where NEL is annotated')
    parser.add_argument('ner_csv', type=str, help='Path to the NER CSV file')
    parser.add_argument('output_file', type=str, help='Path to the output file')
    args = parser.parse_args()

    if not os.path.exists(args.database_file):
        print(f"Database file {args.database_file} does not exist.")
        sys.exit(1)

    if not os.path.exists(args.ner_csv):
        print(f"NER CSV file {args.ner_csv} does not exist.")
        sys.exit(1)

    print("Reading database", args.database_file)
    annotations = read_database(args.database_file, args.project_id)

    df = pd.read_csv(args.ner_csv, sep='\t', keep_default_na=False)
    df['nel'] = ''

    print("Reading", len(annotations), "annotations")
    df = make_dataframe(annotations, df)

    print("Writing to", args.output_file)
    with open(args.output_file, 'w', encoding='utf-8') as f:
        df.to_csv(f, sep='\t')

if __name__ == "__main__":
    main(sys.argv)
