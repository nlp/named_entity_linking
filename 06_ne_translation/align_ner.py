# -*- coding: utf-8 -*-
from sentence_transformers import SentenceTransformer
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd
import numpy as np
import argparse


def process_all(iob_source, iob_target, model):
    for document_id in sorted(list(set(iob_source['document'].values.tolist()))):
        sentences_source = iob_source.loc[iob_source['document'] == document_id]
        sentences_target = iob_target.loc[iob_target['document'] == document_id]
        #        print("document_id", document_id)
        yield document_id, process_document(sentences_source, sentences_target, model)


def process_document(sentences_source, sentences_target, model):
    num_sentences = sentences_source['index'].max()
    #    print("num sentences", num_sentences)
    translations = []
    for sentence_id in range(1, num_sentences):
        sentence_source = sentences_source.loc[sentences_source['index'] == sentence_id]
        sentence_target = sentences_target.loc[sentences_target['index'] == sentence_id]
        sentence_source_ids = []
        sentence_target_ids = []
        if sentence_source['alignment'].any():
            #print(sentence_source['alignment'].iloc[0])
            alignment = [int(x) for x in sentence_source['alignment'].iloc[0].split(':')]
            for i in range(alignment[0]):
                sentence_source_ids.append(sentence_id - i)
            for i in range(alignment[1]):
                sentence_target_ids.append(sentence_id + i)
        if not sentence_source.empty or not sentence_target.empty:
            #            print(' '.join(sentence_source['token'].values.tolist()))
            #            print(' '.join(sentence_target['token'].values.tolist()))
            sentence_source = sentences_source.loc[sentences_source['index'].isin(sorted(sentence_source_ids))]
            sentence_target = sentences_target.loc[sentences_target['index'].isin(sorted(sentence_target_ids))]
            source_labels = list(set(sentence_source.label.values.tolist()))
            target_labels = list(set(sentence_target.label.values.tolist()))
            #            print("labels", source_labels, target_labels)
            if len(source_labels) > 1 and len(target_labels) > 1:
                #                print("calculate similarity", )
                t = find_similar(sentence_source, sentence_target, model)
                #                for x,y,i,j,s in t:
                #                    print(x,'=',y, '(',s,')', i, j)
                translations.extend(t)
    #            print('-'*40)
    return translations


def get_entities(sentence):
    entities = sentence.loc[sentence['label'] != 'O']
    entities_copy = entities.copy()
    entities_copy['ent'] = (entities_copy.label.str[0] == 'B').cumsum()
    ents = entities_copy.groupby(by='ent').indices
    indices = [entities_copy.iloc[ents[x]]['start'].values.tolist() for x in ents.keys()]
    token_indices = [entities_copy.iloc[ents[x]].index.values.tolist() for x in ents.keys()]
    entities_copy = [' '.join(entities_copy.iloc[ents[x]]['token'].astype(str).values.tolist()) for x in ents.keys()]
    #print("indices", indices)
    #print(type(indices[0]))
    #print("token indices", token_indices)
    #print(type(token_indices[0]))
    #   print(list(zip(entities_copy, indices, token_indices)))
    return zip(entities_copy, indices, token_indices)


def find_similar(sentence_source, sentence_target, model, threshold=0.7):
    entities_source = get_entities(sentence_source)
    entities_target = get_entities(sentence_target)

    #print(list(entities_source))

    tokens_source = []
    indices_source = []
    for x, y, z in entities_source:
        tokens_source.append(x)
        indices_source.append(y)
    tokens_target = []
    indices_target = []
    for x, y, z in entities_target:
        tokens_target.append(x)
        indices_target.append(y)

    translations = []
    source_embeddings = model.encode(tokens_source)  #.reshape(-1, 1)
    target_embeddings = model.encode(tokens_target)  #.reshape(-1, 1)
    similarity = cosine_similarity(source_embeddings, target_embeddings)
    for i in np.argmax(similarity, axis=0):
        j = np.argmax(similarity[i])
        sim = similarity[i, j]
        #        print(i,j, ':', tokens_source[i],'=', tokens_target[j], sim)
        b = similarity[i]
        b[b < threshold] = 0
        b[j] = 0
        similarity[i] = b
        c = similarity[:, j]
        c[c < threshold] = 0
        c[i] = 0
        similarity[:, j] = c
        translations.append((tokens_source[i], tokens_target[j], indices_source[i], indices_target[j], sim))

    return translations


def add_translations(doc_id, iob_source, iob_target, i, j):
    if 'nel' not in iob_target.columns:
        iob_target['nel'] = ''
    #    print("doc_id", doc_id)
    sentence_no = iob_source.loc[(iob_source['document'] == doc_id) & (iob_source['start'].isin(i)), 'index'].values[0]
    qname = iob_source.loc[(iob_source['document'] == doc_id) & (iob_source['start'].isin(i)) & (
                iob_source['index'] == sentence_no), 'nel'].values[0]
    iob_target.loc[(iob_target['document'] == doc_id) & (iob_target['start'].isin(j)) & (
                iob_target['index'] == sentence_no), 'nel'] = qname
    #    print("dcc_id", doc_id)
    #    print("iob_source", iob_source[iob_source.document==doc_id])
    #    print("translations", translations)
    return iob_target


def main():
    parser = argparse.ArgumentParser(description='Process source and target TSV files with NER/NEL annotation, aligns NER, possibly transfers NEL annotation to target language.')
    parser.add_argument('source_iob_data_tsv', type=str, help='Path to the source TSV data for all documents ')
    parser.add_argument('target_iob_data_tsv', type=str, help='Path to the target TSV data for all documents ')
    parser.add_argument('--output_nel_tsv', type=str, help='Path to save the output NEL TSV file', default=None)
    parser.add_argument('--translations_file', type=str, help='Path to the file containing translations', default=None)
    parser.add_argument('--threshold', type=float, help='Threshold for similarity', default=0.7)

    args = parser.parse_args()

    source_tsv = args.source_iob_data_tsv
    target_tsv = args.target_iob_data_tsv

    print("Reading model")

    model = SentenceTransformer('sentence-transformers/distiluse-base-multilingual-cased-v2')

    print("Reading data in path", source_tsv, "and", target_tsv)

    iob_source = pd.read_csv(source_tsv, sep='\t', keep_default_na=False)
    iob_target = pd.read_csv(target_tsv, sep='\t', keep_default_na=False)
    translations = process_all(iob_source, iob_target, model)

    f = None
    if args.translations_file:
        f = open(args.translations_file, 'w', encoding='utf-8')

    for doc_id, sentence in translations:
        for x, y, i, j, s in sentence:

            if f:
                if s > args.threshold:
                    f.write(doc_id + '\t' + x + '\t' + y + '\t' + str(i) + '\t' + str(j) + '\t' + str(s) + '\n')
            else:  # output all alignments to stdout
                print(x, '\t', y, '\t', i, j, s)
            if args.output_nel_tsv and 'nel' in iob_source.columns and s > args.threshold:
                iob_target = add_translations(doc_id, iob_source, iob_target, i, j)

    if f:
        f.close()

    if args.output_nel_tsv:
        iob_target.to_csv(args.output_nel_tsv, sep='\t')


if __name__ == "__main__":
    main()
